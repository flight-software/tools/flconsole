//=======================================================================================================================
// Written by LAICE software team in Oct, 2013 to simulate the data received from PMTs. 
// Pretend hardware (emulator) is in the loop: External computer / pseudoterminal sends data 
// 1. Emulator sends fake signals through its serial port (maybe via serial-to-USB converter, or maybe through pseudoterminal setup by socat)
// 2. Payload reader daemon on the board receives through its serial port (defined by PORT_NAME)
// 3. Daemon prints data into file every second

// gl_mode stores the current state of the daemon (i.e. idle, science or housekeeping)

// This program runs as a daemon; should be exited using "killall -s SIGTERM PMTd" in the scheduler
//=========================================================================================================================

// Local source files
#include <math.h>
#include <unistd.h>
#include <stdio.h>
#include <errno.h>
#include <string.h>
#include <time.h>

#include "libdebug.h"
#include "libtty.h"
// Signal for closing?

static void timestamp(void);
static double vcal(uint8_t lsb, uint8_t msb);
static double tcal(uint8_t, uint8_t);
static uint16_t iraw(uint16_t, uint16_t);
static double ical74(uint8_t lsb, uint8_t msb);
static double ical33(uint8_t lsb, uint8_t msb);
static int flConsole(int);

int flfd;
char curr_time[32];
//=========================================================================================================================
void timestamp() {
    time_t ltime;
    struct tm result;

    ltime = time(NULL);
    localtime_r(&ltime, &result);
    asctime_r(&result, curr_time);
    curr_time[strlen(curr_time) - 1] = 0;
}
//=========================================================================================================================
/**
 * Voltage calibration from the power board. Currently
 * not known why the order is lsb, msb, even though we 
 * call it in the reverse order. most likely an endianness
 * mismatch we could solve using the proper macros from endian.h,
 * but for now just don't touch it.
 */
double vcal(uint8_t lsb, uint8_t msb) {
    uint16_t raw = iraw(lsb, msb & 0x0F);
    double voltage = (double)raw * 3.0 / 4096.0;
    return voltage;
}

//=========================================================================================================================
/**
 * Temperature calibration from the batteries.
 * Lots of magic numbers, apparently from the
 * datasheet or something. Bird claims its from the
 * Steinhart-Hart equation, but simplified so as not to
 * do a cube on the PICs.
 * */
double tcal(uint8_t lsb, uint8_t msb) {
    double temp = (3.3 / vcal(lsb, msb)) * 100000 - 100000;
    double BETA = 4261.0;
    return (BETA * (25 + 273.15) / (BETA + log(temp / 50000) * (25 + 273.15)) - 273.15);
}

uint16_t iraw(uint16_t lsb, uint16_t msb) {
    uint16_t raw = msb;
    raw = (uint16_t)(raw << 8);
    raw = (uint16_t)(raw | lsb);
    return raw;
}
//=========================================================================================================================
/**
 * Current calibration function for a given hotswap.
 * Calculated using a linear regression from the programmable load.
 * Subject to change for any given hotswap, probably by just making
 * a calibration function for individual hotswaps.
 *
 * Also would be advantageous to precompute these magic numbers,
 * although that's probably optimized by the compiler anyways.
 */
double ical74(uint8_t lsb, uint8_t msb) {
    uint16_t raw = iraw(lsb, msb);

    double current = raw * 3.3 / 4096.0;
    current = current * 0.8594 - 0.0748;        //7.4V channel
    return current;
}

//=========================================================================================================================
/**
 * Current calibration function for a given hotswap.
 * Calculated using a linear regression from the programmable load.
 * Subject to change for any given hotswap, probably by just making
 * a calibration function for individual hotswaps.

 *
 * Also would be advantageous to precompute these magic numbers,
 * although that's probably optimized by the compiler anyways.
 */
double ical33(uint8_t lsb, uint8_t msb) {
    uint16_t raw = iraw(lsb, msb);

    double current = raw * 3.3 / 4096.0;
    current = current * 0.459 - 0.1562;       //3.3V channel
    return current;
}

//=========================================================================================================================
char * flextty = NULL;

int flConsole(int fd) {
   
    char in[100];
    uint8_t output[100];
    char * pt;
    uint8_t cmd[8];
    size_t count = 0;
    fd_set rfds;
    int retval;
    printf("Enabling Flex Cable hotswap\n");

    start_serial(&flfd, flextty, B115200, 1, 10);
    if(rs485_setup(flfd)) {
        fprintf(stderr, "Failed rs485 setup\n");
        exit(EXIT_FAILURE);
    }
    fd = flfd;
    printf("flex cable fd: %d\n",flfd);
    
    while(1) {

        printf("Enter a hex command for the flex cable:\n");
        char* junk = fgets(in, 100, stdin);
        junk ++;
        pt = in;
        for(count = 0; count < sizeof(cmd)/sizeof(cmd[0]); count ++) {
            sscanf(pt, "%2hhx", &cmd[count]);
            pt += 2;
        }
        printf("About to send this message to ADCS: "); print_struct(cmd, 8, 3);
        printf("Attempting to write command...\n");
        tcflush(fd, TCIOFLUSH);
        if(write(fd, cmd, strlen(in)/2) != (ssize_t)(strlen(in)/2)) {
            printf("Write failed. buffer:\n"); print_struct(cmd, strlen(in)/2, 5);
        } else {
            printf("Wrote command successfully.\n");
        }
        FD_ZERO(&rfds);
        struct timeval tv;
        tv.tv_usec = 9e5;
        tv.tv_sec = 0;
        do { 
            FD_SET(fd, &rfds);
            retval = select(fd + 1, &rfds, NULL, NULL, &tv);
            printf("retval: %d %d %d\n", retval, fd, FD_SETSIZE);
            if(FD_ISSET(fd, &rfds)) { // we got some data!
                printf("we got some data! we out this bitch.\n");
                break;
            }
            if(retval == -1 && errno == EINTR) {
                printf("Nothing yet homie. Not to worry, we'll get there.\n");
                continue;
            }
            if(retval == -1) {
                printf("syslog failed horribly.\n");
                syslog(LOG_ERR, "select() failed. Aborting hotswap read");
                return EXIT_FAILURE;
            }
        } while(retval < 0);
        printf("made it out of the do while loop\n");
        usleep(50e3);
        if(FD_ISSET(fd, &rfds) != 0) {
            usleep(0);
        } else {
            printf("Looks like the file descriptor isn't set. You sure the flex cables are on?\n");
            continue;
            return EXIT_FAILURE;
        }
        if(read(fd, output, 30) < 0) {
            printf("Read failed. buffer:\n"); print_struct(output, 30, 10);
        } else {
            printf("Successful flex cable response:\n");
            print_struct(output, 30, 10);
        }
    }
}

//=========================================================================================================================

int main(int argc, char *argv[])
{
    if(argc > 1){
        flextty = strdup(argv[1]);
    } else {
        flextty = strdup("/dev/ttyS3");
    }
    printf("using tty %s for flex cables\n", flextty);
    printf("Entering main loop...\n");
    flConsole(-1);
}

//==========================================================================================================================

